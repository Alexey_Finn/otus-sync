﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;

namespace LiveChat
{
	/// <summary>
	/// Чат
	/// </summary>
	public class Chat : IDisposable
	{
		static readonly object _lock = new object();
		static Semaphore semaphore;
		private Mutex mutex;

		/// <summary>
		/// Название чата
		/// </summary>
		public string Name { get; }

		/// <summary>
		/// Максимальное количество участников
		/// </summary>
		public int MaxMembersCount { get; }

		/// <summary>
		/// Время добавления сообщения в чат (исскуственная задержка)
		/// </summary>
		public TimeSpan ChatInsertionTime { get; }

		/// <summary>
		/// Последняя буква или буква с которой нужно начинать следующее слово 
		/// согласно правилам "игры в слова"
		/// (volatile для предотвращения кэширования)
		/// </summary>
		public volatile char LastChar;

		/// <summary>
		/// Конструктор чата
		/// </summary>
		/// <param name="lastChar">Стартовая буква с которой нужно начинать первое слово</param>
		/// <param name="name">Название</param>
		/// <param name="maxMembersCount">Максимальное количество участников</param>
		/// <param name="chatInsertionTime">Время добавления сообщения в чат (исскуственная задержка)</param>
		public Chat(char lastChar, string name, int maxMembersCount, TimeSpan chatInsertionTime, Mutex systemMutex = null)
		{
			Name = name;
			LastChar = lastChar;
			MaxMembersCount = maxMembersCount;
			ChatInsertionTime = chatInsertionTime;
			Messages = new ObservableCollection<string>();
			semaphore = new Semaphore(MaxMembersCount, MaxMembersCount);

			if (systemMutex != null)
			{
				mutex = systemMutex;
			}
		}

		/// <summary>
		/// Метод для входа в чат
		/// </summary>
		public void Enter()
		{
			semaphore.WaitOne();   
		}

		/// <summary>
		/// Метод для выхода из чата
		/// </summary>
		public void Leave()
		{
			// TODO: Надо как-то сигнализировать о том, что чат-комната освободилась от участника
			// TODO: чтобы другие могли войти
			semaphore.Release();
		}

		/// <summary>
		/// Метод, который пытается добавить новое сообщение, 
		/// если оно удовлетворяет правилам игры
		/// </summary>
		/// <param name="chatBot">Чат-бот, который пытается это сделать</param>
		/// <param name="word">Слово-кандидат для добавления</param>
		/// <returns>true, если слово удовлетворяет правилам и было добавлено, false иначе</returns>
		public bool TryAddWord(ChatBot chatBot, string word)
		{
			// Нам нужны слова, а не пустота
			if (string.IsNullOrEmpty(word))
			{
				return false;
			}

			lock (_lock)
			{
				// TODO: Здесь возникает проблема синхронизации
				// TODO: и в чат попадают слова не подходящие по правилам игры
				// TODO: Надо как-то синхронизировать доступ к чату
				if (LastChar == word.First())
				{
					// Иммитируем долгую операцию...
					Thread.Sleep(ChatInsertionTime);
					// Добавляем слово в список
					Messages.Add($"{chatBot.Name}: {word}");
					// Обновляем последнюю букву
					LastChar = word.Last();
					// Для специальных букв - специальные правила
					switch (LastChar)
					{
						case 'ё':
							LastChar = 'е';
							break;
						case 'й':
							LastChar = 'и';
							break;
						case 'ь':
						case 'ъ':
						case 'ы':
							LastChar = 'а';
							break;
					}
					return true;
				}
				chatBot.State = "Не успел предложить слово";
				return false;
			}
		}

		public void Dispose()
		{
			if (mutex != null)
			{
				mutex.ReleaseMutex();
			}
		}

		/// <summary>
		/// Список сообщений, 
		/// ObservableCollection - это специальная коллекция, которая позволяет
		/// отслеживать добавление и удаление элементов через событие CollectionChanged
		/// </summary>
		public ObservableCollection<string> Messages { get; }
	}
}
